import curses

KEY_ENTER = 13
KEY_BACKSPACE = 127
KEY_NOTHING = 195
KEY_ARING_LC = 165
KEY_ARING_UC = 133
KEY_ADOTS_LC = 164
KEY_ADOTS_UC = 132
KEY_ODOTS_LC = 182
KEY_ODOTS_UC = 150

CHAR_ARING_LC = 229
CHAR_ARING_UC = 194
CHAR_ADOTS_LC = 228
CHAR_ADOTS_UC = 196
CHAR_ODOTS_LC = 246
CHAR_ODOTS_UC = 214

BLUE = 1
CYAN = 2
GREEN = 3
MAGENTA = 4
RED = 5
WHITE = 6
YELLOW = 7

colors = {
    BLUE:       curses.COLOR_BLUE,
    CYAN:       curses.COLOR_CYAN,
    GREEN:      curses.COLOR_GREEN,
    MAGENTA:    curses.COLOR_MAGENTA,
    RED:        curses.COLOR_RED,
    WHITE:      curses.COLOR_WHITE,
    YELLOW:     curses.COLOR_YELLOW}
    
#http://stackoverflow.com/questions/566746/how-to-get-console-window-width-in-python
def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

        ### Use get(key[, default]) instead of a try/catch
        #try:
        #    cr = (env['LINES'], env['COLUMNS'])
        #except:
        #    cr = (25, 80)
    return int(cr[1]), int(cr[0])

class TextUI:
    def __init__(self, stdscr):

        stdscr.clear()
        curses.nonl()

        self.server_window = curses.newwin(curses.LINES-3, curses.COLS, 0, 0)
        self.server_window.border()
        self.server_window.nodelay(1)

        self.input_window = curses.newwin(3, curses.COLS, curses.LINES-3, 0)
        self.input_window.border()
        self.input_window.nodelay(1)
        self.input_window.move(1, 1)
        
        self.server_window.refresh()
        self.input_window.refresh()

        self.server_line = 1
        self.input_pos = 1
        self.current_color = WHITE

        self.user_input = ""
        
        self.last_messages = []
        self.buffer_size = 50

        for number, val in colors.items():
            curses.init_pair(number, colors[number], curses.COLOR_BLACK)
            
    def add_message(self, message, color):
        if len(self.last_messages) == self.buffer_size:
            self.last_messages.pop(0)
        self.last_messages.append((message,color))

    def set_text_color(self, color):
        self.current_color = color

    def handle_enter_key(self, key):
        # Enter
        msg_from_user = self.user_input
        self.user_input = ""
        self.input_pos = 1
        self.input_window.clear()
        self.input_window.border()
        self.input_window.move(1, 1);
        self.input_window.refresh()
        return msg_from_user
        
    def handle_backspace_key(self, key):
        # Backspace
        if len(self.user_input) == 0:
            return
            
        cols, _ = getTerminalSize()
            
        self.user_input = self.user_input[:-1]
        self.input_pos -= 1
        if self.input_pos == 1 and self.user_input != "":
            self.input_pos = cols-2
            self.input_window.move(1, 1)
            self.input_window.addstr(self.user_input[-(cols-3):])
            self.input_window.move(1, self.input_pos)
        else:
            self.input_window.move(1, self.input_pos)
            self.input_window.addstr(" ")
            self.input_window.move(1, self.input_pos)
        self.input_window.refresh()
        return None
    def handle_normal(self, key):
        key2swedish_char = {KEY_ARING_LC: CHAR_ARING_LC,
                            KEY_ARING_UC: CHAR_ARING_UC,
                            KEY_ADOTS_LC: CHAR_ADOTS_LC,
                            KEY_ADOTS_UC: CHAR_ADOTS_UC,
                            KEY_ODOTS_LC: CHAR_ODOTS_LC,
                            KEY_ODOTS_UC: CHAR_ODOTS_UC}
        if key in key2swedish_char:
            actual_char = chr(key2swedish_char[key])
        else:
            actual_char = chr(key)
            
        cols, _ = getTerminalSize()
            
        self.user_input += actual_char
        self.input_window.move(1, self.input_pos);
        self.input_pos += 1
        if self.input_pos == cols-1:
            self.input_pos = 2
            self.input_window.clear()
            self.input_window.border()
            self.input_window.move(1, 1);
            self.input_window.refresh()
        self.input_window.addstr(actual_char)
        self.input_window.refresh()
        return None
        
    def handle_resize(self, key):
        cols, lines = getTerminalSize()
        self.server_window.clear()
        self.input_window.clear()
        self.server_window.resize(lines-3, cols)
        self.input_window.mvwin(lines-3, 0)
        self.input_window.resize(3, cols)
        self.input_window.move(1, 1)
        self.server_window.border()
        self.input_window.border()
        
        self.server_line = 0
        if self.user_input != "":
            self.input_pos = (len(self.user_input)-1)%(cols-3) + 2
            self.input_window.addstr(self.user_input[-(self.input_pos-1):])
        self.server_window.refresh()
        self.input_window.refresh()
        current_color_backup = self.current_color
        for (msg, color) in self.last_messages:
            self.current_color = color
            self.output(msg, False)
        self.current_color = current_color_backup
        return None
        
    def no_func(self, key):
        return None

    def input(self):
        keyboard_mapping = { -1            : self.no_func,
                             KEY_NOTHING   : self.no_func,
                             KEY_ENTER     : self.handle_enter_key,
                             KEY_BACKSPACE : self.handle_backspace_key,
                             curses.KEY_RESIZE : self.handle_resize}

        key = self.input_window.getch()

        if key in keyboard_mapping:
            return keyboard_mapping[key](key)
        else:
            return self.handle_normal(key)

    def output(self, msg, add = True):
        
        cols, lines = getTerminalSize()
                
        if add:
            self.add_message(msg, self.current_color)

        while msg:           
            
            if self.server_line == (lines - 5):
                self.server_window.move(1,1)
                self.server_window.deleteln()
                self.server_window.move(self.server_line, 1)
                self.server_window.addstr(" " * (cols - 2))
                self.server_window.border()
            else:
                self.server_line += 1

            if len(msg) > cols - 2:
                part = msg[:cols - 3]
                split = part.rfind(' ')
                if split == -1:
                    split = cols - 2
                line = part[:split]
                msg = msg[len(line):]
                msg = msg.lstrip()
            else:
                line = msg
                msg = None

            self.server_window.move(self.server_line, 1)
            self.server_window.addstr(line, curses.color_pair(self.current_color))

            self.input_window.move(1, self.input_pos)
            self.server_window.refresh()
            self.input_window.refresh()
