import pirc
import interface
import curses

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "monty"
REALNAME = "Montezuma II"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)


def main(stdscreen):

    ui = interface.TextUI(stdscreen)

    while True:
        msg = irc.read()
        if msg:
            ui.output(msg)

curses.wrapper(main)
